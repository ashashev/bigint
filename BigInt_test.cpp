/*
 * BigInt_test.cpp
 *
 *  Created on: 05.11.2016
 *      Author: Aleksey Shashev
 */

#include "BigInt.h"
#include "auxiliary.h"

#include "catch/catch.hpp"

template<typename T, typename = std::enable_if_t<std::is_integral<T>::value> >
auto getBasicTestValues()
{
    std::vector<T> values = {
            std::numeric_limits<T>::min(),
            std::numeric_limits<T>::max(),
            std::numeric_limits<T>::max() / 2,
            (std::numeric_limits<T>::max() / 3) * 2,
            std::numeric_limits<T>::min() / 2,
            (std::numeric_limits<T>::min() / 3) * 2,
    };

    return values;
}

TEST_CASE("Construct BigInt from the integral types")
{
    SECTION("from unsigned char")
    {
        auto testValues = getBasicTestValues<unsigned char>();
        for (const auto& test: testValues)
        {
            const BigInt value(test);
            CHECK(static_cast<decltype(test)>(value) == test);

            const BigInt constValue(test);
            CHECK(static_cast<decltype(test)>(constValue) == test);
        }
    }

    SECTION("from unsigned short")
    {
        auto testValues = getBasicTestValues<unsigned short>();
        for (const auto& test: testValues)
        {
            const BigInt value(test);
            CHECK(static_cast<decltype(test)>(value) == test);

            const BigInt constValue(test);
            CHECK(static_cast<decltype(test)>(constValue) == test);
        }
    }

    SECTION("from unsigned int")
    {
        auto testValues = getBasicTestValues<unsigned int>();
        for (const auto& test: testValues)
        {
            const BigInt value(test);
            CHECK(static_cast<decltype(test)>(value) == test);

            const BigInt constValue(test);
            CHECK(static_cast<decltype(test)>(constValue) == test);
        }
    }

    SECTION("from unsigned long long")
    {
        auto testValues = getBasicTestValues<unsigned long long>();
        for (const auto& test: testValues)
        {
            const BigInt value(test);
            CHECK(static_cast<decltype(test)>(value) == test);

            const BigInt constValue(test);
            CHECK(static_cast<decltype(test)>(constValue) == test);
        }
    }

    SECTION("from -1 to unsigned char")
    {
        const BigInt value(-1);
        unsigned char test = value;
        unsigned char expected = 0xFFu;
        CHECK(test == expected);
    }

    SECTION("from -255 to unsigned char")
    {
        const BigInt value(-255);
        unsigned char test = value;
        unsigned char expected = 1;
        CHECK(test == expected);
    }

    SECTION("from -256 to unsigned char")
    {
        const BigInt value(-256);
        unsigned char test = value;
        unsigned char expected = 0u;
        CHECK(test == expected);
    }

    SECTION("from short")
    {
        auto testValues = getBasicTestValues<short>();
        for (const auto& test: testValues)
        {
            const BigInt value(test);
            CHECK(static_cast<decltype(test)>(value) == test);

            const BigInt constValue(test);
            CHECK(static_cast<decltype(test)>(constValue) == test);
        }
    }

    SECTION("from int")
    {
        auto testValues = getBasicTestValues<int>();
        for (const auto& test: testValues)
        {
            const BigInt value(test);
            CHECK(static_cast<decltype(test)>(value) == test);

            const BigInt constValue(test);
            CHECK(static_cast<decltype(test)>(constValue) == test);
        }
    }

    SECTION("from long")
    {
        auto testValues = getBasicTestValues<long>();
        for (const auto& test: testValues)
        {
            const BigInt value(test);
            CHECK(static_cast<decltype(test)>(value) == test);

            const BigInt constValue(test);
            CHECK(static_cast<decltype(test)>(constValue) == test);
        }
    }

    SECTION("from long long")
    {
        auto testValues = getBasicTestValues<long long>();
        for (const auto& test: testValues)
        {
            const BigInt value(test);
            CHECK(static_cast<decltype(test)>(value) == test);

            const BigInt constValue(test);
            CHECK(static_cast<decltype(test)>(constValue) == test);
        }
    }
}

TEST_CASE("Getting string from BigInt")
{
    SECTION("Default constructor")
    {
        const BigInt value;
        std::string expected("0");
        CHECK(value.toString() == expected);
    }

    SECTION("form 127")
    {
        const BigInt value(127u);
        std::string expected("127");
        CHECK(value.toString() == expected);
    }

    SECTION("form 65536")
    {
        const BigInt value(65536u);
        std::string expected("65536");
        CHECK(value.toString() == expected);
    }

    SECTION("Some values")
    {
        auto testValues = getBasicTestValues<std::uint64_t>();
        for (const auto& test: testValues)
        {
            const std::string expected(std::to_string(test));

            const BigInt value(test);
            CHECK(value.toString() == expected);

            const BigInt constValue(test);
            CHECK(constValue.toString() == expected);
        }
    }
}

TEST_CASE("Construct BigInt from string")
{
    SECTION("from 0")
    {
        const std::string test("0");
        const BigInt value(test);
        CHECK(value.toString() == test);
    }

    SECTION("from wrong string \"123w312\"")
    {
        const std::string test("123w312");
        const BigInt value(test);
        const std::string expected("0");
        CHECK(value.toString() == expected);
    }

    SECTION("from 127")
    {
        const std::string test("127");
        const BigInt value(test);
        CHECK(value.toString() == test);
    }

    SECTION("from -127")
    {
        const std::string test("-127");
        const BigInt value(test);
        CHECK(value.toString() == test);
    }

    SECTION("from 123456789012345678901234567890")
    {
        const std::string test("123456789012345678901234567890");
        const BigInt value(test);
        CHECK(value.toString() == test);
    }

    SECTION("from some values")
    {
        auto testValues = getBasicTestValues<std::uint64_t>();
        for (const auto& test: testValues)
        {
            const std::string expected(std::to_string(test));

            const BigInt value(expected);
            CHECK(value.toString() == expected);
        }
    }
}

TEST_CASE("comparison of BigInt")
{
    SECTION("compare with self")
    {
        const BigInt value(1232);
        CHECK(value == value);
    }

    SECTION("compare equal values")
    {
        const BigInt v1(1232);
        BigInt v2(1232);
        CHECK(v1 == v2);
    }

    SECTION("compare equal negative values")
    {
        const BigInt v1(-1232);
        BigInt v2(-1232);
        CHECK(v1 == v2);
    }

    SECTION("compare different values")
    {
        const BigInt v1(1232);
        BigInt v2(6521);
        CHECK_FALSE(v1 == v2);
    }

    SECTION("compare zero and not zero")
    {
        BigInt v1(0);
        BigInt v2(123);
        BigInt v3(-123);
        CHECK_FALSE(v1 == v2);
        CHECK_FALSE(v1 == v3);
        CHECK_FALSE(v2 == v3);
    }

    SECTION("compare equal value which were made from ulong long and ushort")
    {
        unsigned long long s1 = 123;
        unsigned short s2 = 123;
        CHECK(s1 == s2);
        BigInt v1(s1);
        BigInt v2(s2);
        CHECK(v1 == v2);
    }

    SECTION("compare values created from ulong long and ushort")
    {
        unsigned long long s1 = 123;
        unsigned short s2 = 250;
        CHECK(s1 < s2);
        BigInt v1(s1);
        BigInt v2(s2);
        CHECK(v1 < v2);
        CHECK(v2 > v1);
    }

    SECTION("compare 1234 and 2341")
    {
        BigInt v1(1234);
        BigInt v2(2341);
        CHECK(v1 < v2);
        CHECK_FALSE(v2 < v1);
        CHECK(v2 > v1);
    }

    SECTION("compare -1234 and -423")
    {
        BigInt v1(-1234);
        BigInt v2(-423);
        CHECK_FALSE(v1 == v2);
        CHECK(v1 < v2);
        CHECK_FALSE(v2 < v1);
        CHECK(v2 > v1);
    }

    SECTION("compare -523, 0 and 523")
    {
        BigInt v1(-523);
        BigInt v2(0);
        BigInt v3(523);

        CHECK(v1 < v2);
        CHECK(v2 < v3);
        CHECK(v1 < v3);
        CHECK(-v1 == v3);
        CHECK(v1 != v2);
        CHECK(v3 != v2);
    }
}

TEST_CASE("Check sum and subtract with BigInt")
{
    SECTION("Positive numbers")
    {
        BigInt v1(100000);
        BigInt v2(90000);
        CHECK(v2 + 0 == v2);
        CHECK(v2 - 0 == v2);
        CHECK(v1 + v2 == 190000);
        CHECK(v1 - v2 == 10000);
        CHECK(v2 - v1 == -10000);
    }

    SECTION("Negative numbers")
    {
        BigInt v1(-100000);
        BigInt v2(-90000);
        CHECK(v2 + 0 == v2);
        CHECK(v1 + v2 == -190000);
        CHECK(v1 - v2 == -10000);
        CHECK(v2 - v1 == 10000);
    }
}
