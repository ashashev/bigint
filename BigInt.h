/*
 * BigInt.h
 *
 *  Created on: 05.11.2016
 *      Author: Aleksey Shashev
 */

#ifndef BIGINT_H_
#define BIGINT_H_

#include "auxiliary.h"

#include <type_traits>
#include <limits>
#include <vector>
#include <string>
#include <algorithm>
#include <cstring>
#include <cmath>

class BigInt final
{
    using Type = unsigned short;
    using ExtType = unsigned int;

    static_assert(aux::isUnsigned<Type>(),
            "Type should be unsigned integral");

    static_assert(aux::isUnsigned<ExtType>(),
            "Type should be unsigned integral");

    static_assert(sizeof(ExtType) >= (2 * sizeof(Type)),
            "Size of ExtType should be twice many than size of Type");
public:
    BigInt();

    explicit BigInt(const std::string& str);

    template<typename T, typename = std::enable_if_t< std::is_integral<T>::value > >
    BigInt(T value):
        BigInt(value, aux::isUnsignedT<T>())
    {
    }

    template<typename T, typename = std::enable_if_t< std::is_integral<T>::value > >
    operator T () const
    {
        T value = 0;
        std::memcpy(&value, data_.data(),
                std::min(sizeof(T), sizeof(Type) * data_.size())
        );

        if (negative_)
            value *= -1;

        return value;
    }

    std::string toString() const;
    std::wstring toWstring() const;

    bool operator== (const BigInt& that) const;
    bool operator!= (const BigInt& that) const;
    bool operator < (const BigInt& that) const;
    bool operator > (const BigInt& that) const;
    bool operator <= (const BigInt& that) const;
    bool operator >= (const BigInt& that) const;

    BigInt operator- () const;
    BigInt& operator+=(const BigInt& that);
    BigInt& operator-=(const BigInt& that);
private:
    template<typename T>
    BigInt(T value, std::true_type /*unsigned*/) :
        data_(sizeof(T) < sizeof(Type)? 1: sizeof(T) / sizeof(Type)),
        negative_(false)
    {
        std::memcpy(data_.data(), &value, sizeof(value));
        trim();
    }

    template<typename T>
    BigInt(T value, std::false_type /*unsigned*/) :
        data_(sizeof(T) < sizeof(Type)? 1: sizeof(T) / sizeof(Type)),
        negative_(false)
    {
        const T absValue = std::abs(value);
        std::memcpy(data_.data(), &absValue, sizeof(value));
        negative_ = value < 0;
        trim();
    }

    void trim();

    std::vector<Type> data_;
    bool negative_;
};

std::ostream& operator << (std::ostream& os, const BigInt& value);
std::wostream& operator << (std::wostream& os, const BigInt& value);

BigInt operator+ (const BigInt& lhv, const BigInt& rhv);
BigInt operator- (const BigInt& lhv, const BigInt& rhv);

#endif /* BIGINT_H_ */
