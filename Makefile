CXXFLAGS =	-O2 -g -Wall -fmessage-length=0 -std=c++1y

OBJS =		main.o BigInt.o BigInt_test.o

LIBS =

TARGET =	BigInt

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

all:	$(TARGET)

clean:
	rm -f $(OBJS) $(TARGET)

BigInt.o, BigInt_test.o:	BigInt.h auxiliary.h
