/*
 * BigInt.cpp
 *
 *  Created on: 05.11.2016
 *      Author: Aleksey Shashev
 */

#include "BigInt.h"

#include <algorithm>
#include <cassert>
#include <cctype>

namespace
{

template <typename WorkType, typename DataType>
std::vector<WorkType> getDigits(
        const WorkType base,
        const WorkType targetBase,
        const std::vector<DataType>& data)
{
    std::vector<WorkType> out =
    { 0 };

    std::for_each(data.crbegin(), data.crend(),
            [&out, base = base, targetBase = targetBase](auto& value)
            {
                WorkType carry = value;
                for(auto& digits: out)
                {
                    auto cur = digits * base + carry;
                    digits = cur % targetBase;
                    carry = cur / targetBase;
                }

                while (carry > 0)
                {
                    out.push_back(carry % targetBase);
                    carry /= targetBase;
                }
            });

    return out;
}

template<typename Type, typename ExtType>
std::vector<Type> sum(const std::vector<Type>& ldata, const std::vector<Type>& rdata)
{
    static const ExtType base = std::numeric_limits<Type>::max() + 1u;

    std::vector<Type> tmp;
    tmp.reserve(std::max(ldata.size(), rdata.size()));
    ExtType mem = 0;
    auto lhv = ldata.cbegin();
    auto rhv = rdata.cbegin();
    auto lhend = ldata.cend();
    auto rhend = rdata.cend();

    while(lhv != lhend || rhv != rhend)
    {
        if (lhv != lhend)
        {
            mem += *lhv;
            ++lhv;
        }
        if (rhv != rhend)
        {
            mem += *rhv;
            ++rhv;
        }
        tmp.push_back(mem % base);
        mem = mem / base;
    }

    return tmp;
}

template<typename Type, typename ExtType>
std::vector<Type> subtract(const std::vector<Type>& ldata, const std::vector<Type>& rdata)
{
    /* ldata should be bigger or equal than rdata */
    static const ExtType base = std::numeric_limits<Type>::max() + 1u;
    std::vector<Type> res;
    std::vector<Type> tmp(ldata);
    typename std::vector<Type>::size_type i = 0;
    for (; i < rdata.size(); ++i)
    {
        if (tmp[i] >= rdata[i])
        {
            res.push_back(tmp[i] - rdata[i]);
        }
        else
        {
            ExtType mem = base + tmp[i] - rdata[i];
            tmp[i+1] -= 1;
            assert(mem < base);
            res.push_back(mem);
        }
    }

    std::copy(tmp.cbegin() + i, tmp.cend(), std::back_inserter(res));

    return res;
}
} /* anonymous namespace */

BigInt::BigInt() :
        data_(1),
        negative_(false)
{

}

BigInt::BigInt(const std::string& str) :
        negative_(false)
{
    std::vector<unsigned char> src;
    src.reserve(str.size());

    auto it = str.crbegin();
    auto end = str.crend();
    auto preend = end - 1;
    if (*preend == '-' || *preend == '+')
    {
        end = preend;
        negative_ = (*end == '-');
    }

    for(; it != end; ++it)
    {
        if (std::isdigit(*it))
            src.push_back(*it - '0');
        else
            break;
    }

    if (it != end)
    {
        src.clear();
        src.push_back(0);
        negative_ = false;
    }

    auto data = getDigits(static_cast<ExtType>(10u),
            static_cast<ExtType>(std::numeric_limits<Type>::max()) + 1u,
            src);
    std::copy(data.cbegin(), data.cend(), std::back_inserter(data_));

    void trim();
}

std::string BigInt::toString() const
{
    auto digits = getDigits(
            static_cast<ExtType>(std::numeric_limits<Type>::max()) + 1u,
            static_cast<ExtType>(10u),
            data_
            );

    std::string str;
    std::for_each(digits.crbegin(), digits.crend(), [&str](auto& digit)
    {
        str += std::to_string(digit);
    });

    if (negative_)
        str = "-" + str;

    return str;
}

std::wstring BigInt::toWstring() const
{
    auto digits = getDigits(
            static_cast<ExtType>(std::numeric_limits<Type>::max()) + 1u,
            static_cast<ExtType>(10u),
            data_
            );

    std::wstring str;
    std::for_each(digits.crbegin(), digits.crend(), [&str](auto& digit)
    {
        str += std::to_wstring(digit);
    });

    if (negative_)
        str = L"-" + str;

    return str;
}

bool BigInt::operator== (const BigInt& that) const
{
    if (this == &that)
        return true;

    if (negative_ != that.negative_)
        return false;

    if (data_.size() != that.data_.size())
        return false;

    auto res = std::mismatch(data_.cbegin(), data_.cend(),
            that.data_.cbegin(), that.data_.cend());
    return res.first == data_.cend();
}

bool BigInt::operator!= (const BigInt& that) const
{
    return !(*this == that);
}

bool BigInt::operator < (const BigInt& that) const
{
    if (negative_ != that.negative_)
        return negative_;

    if (data_.size() != that.data_.size())
    {
        return negative_
                ? data_.size() > that.data_.size()
                : data_.size() < that.data_.size();
    }

    auto res = std::mismatch(data_.cbegin(), data_.cend(),
            that.data_.cbegin(), that.data_.cend());

    return (res.first != data_.cend()) &&
            (negative_
                ? *res.first > *res.second
                : *res.first < *res.second
                );
}

bool BigInt::operator > (const BigInt& that) const
{
    return that < *this;
}

bool BigInt::operator <= (const BigInt& that) const
{
    return !(*this > that);
}

bool BigInt::operator >= (const BigInt& that) const
{
    return !(*this < that);
}

BigInt BigInt::operator- () const
{
    BigInt tmp(*this);
    tmp.negative_ = !tmp.negative_;
    return tmp;
}

BigInt& BigInt::operator+= (const BigInt& that)
{
    if (negative_ == that.negative_)
    {
        data_ = sum<Type, ExtType>(data_, that.data_);
    }
    else if (that.negative_)
    {
        *this -= -that;
    }
    else
    {
        BigInt tmp(that);
        negative_ = false;
        tmp -= *this;
        *this = tmp;
    }
    trim();
    return *this;
}

BigInt& BigInt::operator-= (const BigInt& that)
{
    if (negative_ != that.negative_)
    {
        data_ = sum<Type, ExtType>(data_, that.data_);
    }
    else if (negative_)
    {
        BigInt tmp(-that);
        negative_ = false;
        tmp -= *this;
        *this = tmp;
    }
    else
    {
        if (*this >= that)
        {
            data_ = subtract<Type, ExtType>(data_, that.data_);
        }
        else
        {
            data_ = subtract<Type, ExtType>(that.data_, data_);
            negative_ = true;
        }
    }

    trim();
    return *this;
}

void BigInt::trim()
{
    while(data_.back() == 0 && data_.size() > 1)
        data_.pop_back();
}

std::ostream& operator << (std::ostream& os, const BigInt& value)
{
    return os << value.toString();
}

std::wostream& operator << (std::wostream& os, const BigInt& value)
{
    return os << value.toWstring();
}

BigInt operator+ (const BigInt& lhv, const BigInt& rhv)
{
    BigInt tmp = lhv;
    tmp += rhv;
    return tmp;
}

BigInt operator- (const BigInt& lhv, const BigInt& rhv)
{
    BigInt tmp = lhv;
    tmp -= rhv;
    return tmp;
}
