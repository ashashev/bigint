/*
 * auxiliary.h
 *
 *  Created on: 05.11.2016
 *      Author: Aleksey Shashev
 */

#ifndef AUXILIARY_H_
#define AUXILIARY_H_

#include <type_traits>
#include <limits>
#include <cstdlib>

namespace aux
{

namespace __details
{

template <typename T, typename G = typename std::is_integral<T>::type >
struct IsUnsigned: public std::integral_constant<bool, std::numeric_limits<T>::min() == 0>
{
};

template<typename T>
struct IsUnsigned<T, std::false_type>: public std::false_type
{

};

}

template<typename T>
constexpr bool isUnsigned()
{
    return __details::IsUnsigned<T>::value;
}

template<typename T>
using isUnsignedT = typename __details::IsUnsigned<T>::type;

} /* namespace aux */


#endif /* AUXILIARY_H_ */
